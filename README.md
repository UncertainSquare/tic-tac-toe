**How to use**

***With ts-node:***

1. Open terminal window in source directory

2. Run `ts-node tic-tac-toe.ts`

***Without ts-node:***

1. Open terminal window in source directory

2. Run `tsc` to compile (with typescript installed, e.g. `yarn add typescript`)

3. Run `cd js`

4. Run `node tic-tac-toe.js`

The computer always starts playing as X (and X always plays first). At the end of a round, it will ask if you want to play again. Subsequent rounds alternate with the computer playing as X and O.

The program "meditates" at the beginning of execution to determine the optimal move for every reachable game board. This takes a few seconds. It would be a lot faster if it re-used previously encountered board states as discussed on September 21st.

Additionally, it would be much faster if it only looked ahead a couple of moves on each turn, using a simple heuristic to determine fitness for branches that aren't known to lead to victory or defeat. This is how most AIs work because games even a little more complicated than tic-tac-toe quickly become very inefficient to analyze completely.
