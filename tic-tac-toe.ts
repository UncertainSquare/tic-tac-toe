import * as readline from "readline";

import { XO, getWinner, isFull, printBoard } from "./board-analysis";
import { parseMove, stringifyMove, prettyPrintMove, parsePrettyMove, nextToPlay } from "./board-moves";

import {
  generateNeutralBoardStateGraph,
  generateBoardStateGraph,
  chooseNextMove
} from "./graph-analysis";

declare const process;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let playingAs: XO = "X";

async function question(message) {
  return new Promise<string>(resolve => {
    rl.question(message + "\n", answer => resolve(answer));
  });
}

async function playGame() {
  console.log("Meditating...");
  const staticGameAnalysis = generateBoardStateGraph(
    generateNeutralBoardStateGraph()
  );

  console.log("\nWelcome to tic-tac-toe!");

  if (playingAs === "X") {
    console.log("I will play first, as X.\n");
  } else {
    console.log("I will play second, as O.\n");
  }

  for (;;) {
    let currentNode = staticGameAnalysis;

    if (playingAs !== currentNode.gameState.toPlay) {
      printBoard(currentNode.gameState.board, str => console.log(str + "\n"));
    }

    while (
      !getWinner(currentNode.gameState.board) &&
      !isFull(currentNode.gameState.board)
    ) {
      if (currentNode.gameState.toPlay === playingAs) {
        const moveStr = chooseNextMove(currentNode);
        console.log(`I play ${prettyPrintMove(parseMove(moveStr))}`);
        currentNode = currentNode.exits[moveStr];
        printBoard(currentNode.gameState.board, str => console.log(str + "\n"));
      } else {
        let answer = await question(
          `${
            nextToPlay[playingAs]
          } to play. What is your move? Answer in the form {letter}-{number}.`
        );

        try {
          const move = stringifyMove(parsePrettyMove(answer));
          if (move in currentNode.exits) {
            currentNode = currentNode.exits[move];
            printBoard(currentNode.gameState.board, str =>
              console.log(str + "\n")
            );
          } else {
            throw "Illegal move!";
          }
        } catch (e) {
          console.log("Invalid input.\n");
        }
      }
    }

    if (isFull(currentNode.gameState.board)) {
      console.log("TIE");
    } else {
      const winner = getWinner(currentNode.gameState.board);
      console.log(
        `${winner} wins (That's ${winner === playingAs ? "me" : "you"}!)`
      );
    }

    let goAgain;
    do {
      const goAgainStr = (await question("Go again? (Y/N)\n")).toUpperCase();

      if (goAgainStr === "Y" || !goAgainStr) {
        goAgain = true;
      } else if (goAgainStr === "N") {
        goAgain = false;
      } else {
        console.log("Did not understand input.");
      }
    } while (typeof goAgain === "undefined");

    if (!goAgain) {
      console.log("Bye!");
      break;
    } else {
      playingAs = nextToPlay[playingAs];
    }
  }

  rl.close();
}

playGame();
