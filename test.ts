export function assert(bool: boolean, message: string): boolean {
  if (bool) {
    console.log("✓");
  } else {
    console.log(`✗ ${message}`);
  }

  return bool;
}

export function assertThrows(
  fn: Function,
  args: any[],
  message: string
): boolean {
  try {
    fn(...args);
    assert(false, message);
    return false;
  } catch (e) {
    assert(true, message);
    return true;
  }
}
