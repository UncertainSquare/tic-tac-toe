import { assert } from "./test";
import { getWinner, isFull, lookUp } from "./board-analysis";

console.log("getWinner");
assert(
  !getWinner([
    [undefined, undefined, undefined],
    [undefined, undefined, undefined],
    [undefined, undefined, undefined]
  ]),
  ""
);

assert(!getWinner([["X", "O", "X"], ["X", "O", "X"], ["O", "X", "O"]]), "");

assert(
  "X" ===
    getWinner([
      ["X", "O", "X"],
      ["X", undefined, undefined],
      ["X", undefined, undefined]
    ]),
  ""
);

assert(
  "O" === getWinner([["X", "O", "O"], ["O", "O", "X"], ["O", "X", "X"]]),
  ""
);

console.log("isFull");

assert(
  !isFull([
    ["X", "O", "X"],
    ["X", undefined, undefined],
    ["X", undefined, undefined]
  ]),
  ""
);

assert(isFull([["X", "O", "O"], ["O", "O", "X"], ["O", "X", "X"]]), "");

console.log("lookUp");

assert(
  !lookUp(
    [["X", "O", "X"], ["X", undefined, undefined], ["X", undefined, undefined]],
    [1, 1]
  ),
  ""
);

assert(
  lookUp([["X", "O", "O"], ["O", "O", "X"], ["O", "X", "X"]], [2, 2]) === "X",
  ""
);
