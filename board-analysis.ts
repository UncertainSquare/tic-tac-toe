export type XO = "X" | "O";
export type cell = XO | undefined;
export type boardTriplet = [cell, cell, cell];
export type board = [boardTriplet, boardTriplet, boardTriplet];

function tripletHas(boardTriplet: boardTriplet, XO: XO): boolean {
  return boardTriplet.includes(XO);
}

function tripletFull(boardTriplet): boolean {
  return !boardTriplet.includes(undefined);
}

function winnerOfTriplet(boardTriplet: boardTriplet): XO | undefined {
  if (tripletFull(boardTriplet)) {
    const hasX = tripletHas(boardTriplet, "X");
    const hasO = tripletHas(boardTriplet, "O");

    if (hasX && hasO) {
      return undefined;
    } else if (hasX) {
      return "X";
    } else {
      return "O";
    }
  }
}

export function lookUp(board: board, [x, y]: [number, number]): cell {
  return board[y][x];
}

export function isFull(board: board) {
  return !board.find(boardRow => boardRow.includes(undefined));
}

export function getWinner(board: board): XO | undefined {
  for (let boardRow of board) {
    const winner = winnerOfTriplet(boardRow);
    if (!!winner) {
      return winner;
    }
  }

  for (let i = 0; i < board.length; i++) {
    const boardColumn = board.map(boardRow => boardRow[i]) as boardTriplet;
    const winner = winnerOfTriplet(boardColumn);
    if (!!winner) {
      return winner;
    }
  }

  for (let reverse of [true, false]) {
    const boardDiagonal = [0, 1, 2].map(index =>
      lookUp(board, [index, reverse ? 2 - index : index])
    ) as boardTriplet;
    const winner = winnerOfTriplet(boardDiagonal);
    if (!!winner) {
      return winner;
    }
  }
}

export function freshBoard(): board {
  return new Array(3)
    .fill(null)
    .map(_ => new Array(3).fill(undefined) as [cell, cell, cell]) as board;
}

const cellToStr = (cell: cell) => (cell ? cell : "_");

export function printBoard(board: board, outFn: (str) => void) {
  const core = board.map((triplet, index) => `${["A", "B", "C"][index]}  ${triplet.map(cellToStr).join(" ")}`);
  outFn(["   0 1 2"].concat(core).join("\n"));
}
