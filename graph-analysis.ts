import {
  gameState,
  allPossibleMoves,
  stringifyMove,
  newGameState,
  isLegalMove
} from "./board-moves";

import { getWinner, isFull, XO, freshBoard } from "./board-analysis";

import { inPlaceMapObj, avg } from "./utils";

export type neutralNode = {
  gameState: gameState;
  exits: { [move: string]: neutralNode };
};

export type node = neutralNode & {
  fitness: number;
  exits: { [move: string]: node };
};

export function withNeutralExits(neutralNode: neutralNode): neutralNode {
  if (
    !getWinner(neutralNode.gameState.board) &&
    !isFull(neutralNode.gameState.board)
  ) {
    allPossibleMoves
      .filter(move => isLegalMove(neutralNode.gameState.board, move))
      .forEach(move => {
        neutralNode.exits[stringifyMove(move)] = withNeutralExits({
          gameState: newGameState(neutralNode.gameState, move),
          exits: {}
        });
      });
  }

  return neutralNode;
}

// Fitness is X-biased
export function generateBoardStateGraph(neutralNode: neutralNode): node {
  const potentialWinner = getWinner(neutralNode.gameState.board);
  let node: node;
  if (potentialWinner) {
    const fitness = "X" === potentialWinner ? 1000 : -1000;
    node = Object.assign(neutralNode, { fitness, exits: {} });
  } else if (isFull(neutralNode.gameState.board)) {
    node = Object.assign(neutralNode, { fitness: 0, exits: {} });
  } else {
    const exits = inPlaceMapObj(neutralNode.exits, neutralNode =>
      generateBoardStateGraph(neutralNode)
    );

    node = Object.assign(neutralNode, {
      fitness: avg(
        Object.values((neutralNode as node).exits).map(n => n.fitness)
      ),
      exits
    });
  }

  return node;
}

export function generateNeutralBoardStateGraph(): neutralNode {
  const initialGameState = {
    toPlay: "X",
    board: freshBoard()
  } as gameState;

  return withNeutralExits({
    gameState: initialGameState,
    exits: {}
  });
}

export function chooseNextMove(node: node): string {
  const fitnessMultiplier = node.gameState.toPlay === "X" ? 1 : -1;
  const highPair = Object.entries(node.exits).reduce(
    ([moveStr, score], [key, { fitness }]) => {
      const trueFitness = fitness * fitnessMultiplier; // From my perspective the Jedi are evil!
      return score > trueFitness ? [moveStr, score] : [key, trueFitness];
    },
    ["", -Number.MAX_VALUE]
  ) as [string, number];

  const [moveStr] = highPair;
  if (moveStr) {
    return moveStr;
  } else {
    throw "Could not find a move to play";
  }
}
