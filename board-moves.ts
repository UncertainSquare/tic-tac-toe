import { XO, board, lookUp } from "./board-analysis";
import { clone } from "./utils";
import { crossJoinArray } from "./utils";

export type gameState = {
  toPlay: XO;
  board: board;
};

export const nextToPlay = {
  X: "O",
  O: "X"
} as { [key in XO]: XO };

export function isLegalMove(board: board, [x, y]: [number, number]) {
  return !lookUp(board, [x, y]);
}

export function newGameState(
  gameState: gameState,
  [x, y]: [number, number]
): gameState {
  if (isLegalMove(gameState.board, [x, y])) {
    const newGameState = clone(gameState);
    newGameState.board[y][x] = newGameState.toPlay;
    newGameState.toPlay = nextToPlay[gameState.toPlay];
    return newGameState;
  } else {
    throw "Illegal move!";
  }
}

export function stringifyMove([x, y]: [number, number]) {
  return `${x}-${y}`;
}

export function prettyPrintMove([x, y]: [number, number]) {
  return `${["A", "B", "C"][y]}-${x}`;
}

export function parseMove(move: string): [number, number] {
  const results = move
    .match(/^([0|1|2])-([0|1|2])$/)
    .slice(1);
  if (results.length === 2) {
    return results as any as [number, number];
  } else {
    throw "Matched wrong number of things";
  }
}

export function parsePrettyMove(move: string): [number, number] {
  const results = move
    .match(/^([A|a|B|b|C|c])-?([0|1|2])$/)
    .slice(1);
  if (results.length === 2) {
    const y = ["A", "B", "C"].indexOf(results[0].toUpperCase());
    return [Number(results[1]), y];
  } else {
    throw "Matched wrong number of things";
  }
}

export const allPossibleMoves = crossJoinArray([0, 1, 2], [0, 1, 2]);
