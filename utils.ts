import * as _cloneDeep from "lodash.clonedeep";
export const clone = _cloneDeep;

export function flatten(arr: any[]) {
  return arr.reduce((acc, val) => acc.concat(val), []);
}

export function crossJoinArray(arr1: any[], arr2: any[]): [any, any][] {
  return flatten(arr1.map(el1 => arr2.map(el2 => [el1, el2])));
}

export function avg(arr: number[]) {
  return arr.reduce((curr, next) => curr + next / arr.length, 0);
}

export function inPlaceMapObj<T, D>(
  obj: { [key: string]: any },
  fn: (T) => D
): { [key: string]: D } {
  for (let [key, val] of Object.entries(obj)) {
    obj[key] = fn(val);
  }

  return obj;
}

import * as _isEqual from "lodash.isEqual";
export const isEqual = _isEqual;
