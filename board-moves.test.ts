import {
  gameState,
  allPossibleMoves,
  isLegalMove,
  stringifyMove,
  parseMove,
  newGameState
} from "./board-moves";
import { isEqual } from "./utils";
import { assert, assertThrows } from "./test";

console.log("allPossibleMoves");
assert(allPossibleMoves.length === 9, "");

console.log("isLegalMove");
assert(
  isLegalMove(
    [["X", "X", undefined], ["O", "O", "X"], ["X", "O", "X"]],
    [2, 0]
  ),
  ""
);

assert(
  !isLegalMove(
    [["X", "X", undefined], ["O", "O", "X"], ["X", "O", "X"]],
    [0, 0]
  ),
  ""
);

console.log("stringifyMove");

assert(stringifyMove([0, 0]) === "0-0", "");

console.log("parseMove");

assert(isEqual(parseMove("A-0"), [0, 0]), "");

assertThrows(parseMove, ["A-"], "");

assertThrows(parseMove, ["A-3"], "");

assertThrows(parseMove, ["A-2"], "");

console.log("newGameState");

const originalGameState: gameState = {
  toPlay: "X",
  board: [
    [undefined, undefined, undefined],
    [undefined, undefined, undefined],
    [undefined, undefined, undefined]
  ]
};

const newGame = newGameState(originalGameState, [0, 0]);

assert(newGame.board[0][0] === "X", "");
assert(!newGame.board[0][1], "");
assert(!newGame.board[1][0], "");
assert(newGame.toPlay === "O", "");
assert(!originalGameState.board[0][0], "");
