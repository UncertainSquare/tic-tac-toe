import {
  withNeutralExits,
  generateBoardStateGraph,
  generateNeutralBoardStateGraph,
  chooseNextMove,
  neutralNode
} from "./graph-analysis";

import { isEqual } from "./utils";

import { assert } from "./test";

console.log("withNeutralExits");
let wne: neutralNode;
wne = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "X",
    board: [["X", "X", "O"], ["O", "O", "X"], ["X", "O", "X"]]
  }
});

assert(!Object.keys(wne.exits).length, "");

wne = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "X",
    board: [["X", "X", "X"], [undefined, undefined, undefined], ["X", "O", "X"]]
  }
});

assert(!Object.keys(wne.exits).length, "");

wne = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "X",
    board: [["X", "X", "O"], [undefined, undefined, undefined], ["X", "O", "X"]]
  }
});

assert(
  isEqual(new Set(Object.keys(wne.exits)), new Set(["0-1", "1-1", "2-1"])),
  ""
);
assert(!Object.keys(wne.exits["0-1"].exits).length, "");
assert(!Object.keys(wne.exits["1-1"].exits).length, "");
assert(
  isEqual(
    new Set(Object.keys(wne.exits["2-1"].exits)),
    new Set(["0-1", "1-1"])
  ),
  ""
);

console.log("generateBoardStateGraph");
const nbsgTie = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "X",
    board: [["X", "X", "O"], ["O", "O", "X"], ["X", "O", "X"]]
  }
});

const bsgTie = generateBoardStateGraph(nbsgTie);

const nbsgXWins = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "X",
    board: [["X", "X", "X"], ["O", "O", "X"], ["X", "O", "X"]]
  }
});

const bsgXWins = generateBoardStateGraph(nbsgXWins);

const nbsgOWins = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "X",
    board: [["X", "X", "O"], ["O", "O", "O"], ["X", "O", "X"]]
  }
});

const bsgOWins = generateBoardStateGraph(nbsgOWins);

assert(bsgXWins.fitness > bsgTie.fitness, "");
assert(bsgTie.fitness > bsgOWins.fitness, "");

let nbsg = withNeutralExits({
  exits: {},
  gameState: {
    toPlay: "O",
    board: [
      ["X", undefined, undefined],
      ["O", "X", "O"],
      [undefined, undefined, undefined]
    ]
  }
});

console.log(chooseNextMove(generateBoardStateGraph(nbsg)));
